﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LearnMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var obj = new ArquivoViewModelo() { Origem = "origem.html", Destino = "destino.html" };

            return View(obj);
        }

        [HttpPost]
        public ActionResult Executar( ArquivoViewModelo arquivo)
        {
            //var caminho = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;

            //Console.WriteLine(arquivo.Origem);
            //Console.WriteLine(arquivo.Destino);


            return RedirectToAction("Confirmacao", new { arquivo.Destino });
        }

        public ActionResult Confirmacao(string destino)
        {
            ViewBag.Arquivo = destino;
            return View();
        }

        public ActionResult Sobre()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}