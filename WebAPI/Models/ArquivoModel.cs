﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class ArquivoModel
    {
        public string Origem { get; set; }
        public string Destino { get; set; }
    }
}